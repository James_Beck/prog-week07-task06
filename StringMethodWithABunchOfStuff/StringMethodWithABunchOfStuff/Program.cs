﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringMethodWithABunchOfStuff
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 0;
            for (int i = 0; i < 5; i++)
            {
                do
                {
                    Console.WriteLine("Enter a non-zero integer.");
                    try
                    {
                        number = int.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        Console.WriteLine("Not a valid input!");
                        Console.Clear();
                    }
                } while (number == 0);
                Console.WriteLine(stuff(number));
            }     
        }

        static string stuff(int number)
        {
            var output = string.Empty;
            
            if (number % 2 == 0)
            {
                output = "The output is even.";
            }
            else
            {
                output = "The output is odd... Like your mum, OH!!! Sorry for the cyber bullying.";
            }
            return output;
        }
    }
}
